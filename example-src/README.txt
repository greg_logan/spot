This code has the following dependencies.

Gstreamer-Java, from http://code.google.com/p/gstreamer-java/downloads/list
- Gstreamer has also to be installed for that to work, but I am assuming that both these are set up already

JNA (used by Gstreamer-Java), which I just installed using sudo apt-get install libjna-java libjna-java-doc
- one can also probably get it from https://github.com/twall/jna

For serial port I/O, we also need RXTX from http://rxtx.qbang.org/wiki/index.php/Download
