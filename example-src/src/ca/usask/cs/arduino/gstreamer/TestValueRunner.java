package ca.usask.cs.arduino.gstreamer;

public class TestValueRunner {
	private static final int SLEEP_MILLIS = 200;
	
	public static void main(String[] args) {
		BytePacketSender server = new AggressiveUdpServer(17777);
		
		for( byte b = 0; ; b += 16) {
			
			server.sendRawByte( b );
			
			try {
				Thread.sleep(SLEEP_MILLIS);
			} catch (InterruptedException e) {
				// we don't care
			}
		}
	}
}
