package ca.usask.cs.arduino.gstreamer;

public class MinimalRunner {

	public static void main(String[] args) {

		( new PipelineArduinoSink(new AggressiveUdpServer(17777)) )
			.playPipeline();
		
	}

}
