package ca.usask.cs.arduino.gstreamer.example;

import java.nio.IntBuffer;

import org.gstreamer.Buffer;
import org.gstreamer.Element;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.Pipeline;
import org.gstreamer.State;
import org.gstreamer.elements.AppSink;

public class TestPipelining {

	public static void main(String[] args) {
		/*
		 
		 gst-launch filesrc location = "/home/anqi/Downloads/toccata.wav" 
		 ! wavparse !  audioconvert ! audioresample ! monoscope ! 
		 ffmpegcolorspace ! autovideosink
		 
		 */
		
		args = Gst.init("TestPipelining", args);
		
		Pipeline pipeline = Pipeline.launch(
				" audiotestsrc " +
//				"filesrc location = \"/home/anqi/Downloads/toccata.wav\" " +
//				" ! wavparse !  audioconvert ! audioresample " +
				" ! tee name = t ! " +
				" queue ! monoscope ! ffmpegcolorspace ! autovideosink t. " +
				" ! queue ! alsasink t. ! audio/x-raw-int ! queue name=lazip" /* +
				" ! appsink name=sinksink sync=False " */ );
		
/*		Element src = ElementFactory.make("audiotestsrc", "Source");
		Element snk = ElementFactory.make("autoaudiosink", "Dest");
		pipeline.addMany(src, snk);
		src.link(snk);*/
		
		Element attachPoint = pipeline.getElementByName("lazip");
		
		AppSink sink = (AppSink) ElementFactory.make("appsink", "The AppSink");
		pipeline.add(sink);
		attachPoint.link(sink);
		
		pipeline.setState(State.PLAYING);

//		Gst.main();
		
		while( ! sink.isEOS() ) {
			Buffer buf = sink.pullBuffer();
			IntBuffer bBuf = buf.getByteBuffer().asIntBuffer().asReadOnlyBuffer();
//			int[] values = bBuf.array();
			
			for( int i = 0; bBuf.hasRemaining(); i++ ) {
				if( i % 10 == 0 ) {
					System.out.println( bBuf.get() );
				}
			}
		}

		pipeline.setState(State.NULL);
		
	}

}
