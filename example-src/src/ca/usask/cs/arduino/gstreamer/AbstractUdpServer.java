package ca.usask.cs.arduino.gstreamer;

import java.net.DatagramSocket;
import java.net.SocketException;

public abstract class AbstractUdpServer implements BytePacketSender {
	protected final DatagramSocket socket;
	
	/**
	 * 
	 * @param port
	 *            The port on this machine with which to establish a connection.
	 */
	public AbstractUdpServer( int port ) {
		try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			throw new IllegalStateException(
					"Socket at port "+port+" couldn't be opened.", e);
		}
	}
	
	@Override
	public boolean send(String s) {
		return send(s.getBytes());
	}
	
	@Override
	public boolean sendRawByte(byte b) {
		return send(new byte[]{b});
	}
	
	@Override
	public abstract boolean send(byte[] arr);
}
