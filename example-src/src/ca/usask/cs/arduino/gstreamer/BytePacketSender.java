package ca.usask.cs.arduino.gstreamer;

public interface BytePacketSender {

	boolean sendRawByte(byte b);
	boolean send(byte[] arr);
	boolean send(String s);

}
