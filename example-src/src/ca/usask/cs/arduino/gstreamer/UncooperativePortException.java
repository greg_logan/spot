package ca.usask.cs.arduino.gstreamer;

public class UncooperativePortException extends RuntimeException {
	private static final long serialVersionUID = 4432408688007535102L;

	public UncooperativePortException() {
		super();
	}
	
	public UncooperativePortException(String s) {
		super(s);
	}
	
	public UncooperativePortException(Throwable t) {
		super(t);
	}
	
	public UncooperativePortException(String s, Throwable t) {
		super(s, t);
	}
}