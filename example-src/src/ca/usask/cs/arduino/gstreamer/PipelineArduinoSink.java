package ca.usask.cs.arduino.gstreamer;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.gstreamer.Buffer;
import org.gstreamer.Element;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.Pipeline;
import org.gstreamer.State;
import org.gstreamer.elements.AppSink;

public class PipelineArduinoSink {
	private static final int WAIT_MILLIS = 66;
	
	private final Pipeline pipe;
	private final AppSink appSink;
	private final BytePacketSender packetSender;

	public PipelineArduinoSink(BytePacketSender bps) {

		Gst.init("TestPipelining", new String[0]);
		
		/*
		 * I am doing this partially because it's easier, and partially because
		 * I can't figure out how to create all these elements programmatically
		 */
		Pipeline pipeline = Pipeline.launch(
//				" audiotestsrc " +
				"filesrc location = \"/home/anqi/Music/pearl_harbor.wav\" " +
				"! wavparse !  audioconvert ! audioresample " +
				"! tee name = t " +
//				"! queue ! monoscope ! ffmpegcolorspace ! autovideosink " +
				" t. " + "! queue ! alsasink t. ! audio/x-raw-int," +
				" width = 16, depth = 16, signed = true, endianness = 1234 " +
				"! queue name=lazip");

		
		Element attachPoint = pipeline.getElementByName("lazip");
		
		AppSink sink = (AppSink) ElementFactory.make("appsink", "The AppSink");
		pipeline.add(sink);
		attachPoint.link(sink);
		
		this.appSink = sink;
		this.pipe = pipeline;  
		this.packetSender = bps;
	}
	
	public void playPipeline() {
		pipe.setState(State.PLAYING);
		
		(new Thread(new RmsTracker(), "Amplitude finder")).start();
		
		(new Thread(new AmplitudeSender(), "Amplitude value sender")).start();
	}
	
	volatile int currAmplitude = 0;
	volatile double currRms = 0;
	
	/**
	 * Find the current maximum amplitude of the currently playing audio stream
	 * in the pipeline.
	 * 
	 * @deprecated This method was intended to find the current maximum
	 *             amplitude, but it doesn't work as intended.
	 */
	@Deprecated
	private class AmplitudeTracker implements Runnable {
		private int min = 0, max = 0;
		
		@Override
		public void run() {
			boolean lastValPositive = true;
			
			while( ! appSink.isEOS() ) {
				Buffer buf = appSink.pullBuffer();
				IntBuffer bBuf = buf.getByteBuffer().
						asIntBuffer().asReadOnlyBuffer();
				
				for( int i = 0; bBuf.hasRemaining(); i++ ) {
					int val = bBuf.get();
					
					if( val > max ) {
						max = val;
					}
					
					if( val < min ) {
						min = val;
					}
					
					// okay, amplitude going downward
					if( val >= 0 ) {
						if(!lastValPositive) {
							assert( max > 0 );
							currAmplitude = max;
						}
						
						lastValPositive = true;
						max = 0;
					} else {
						assert( val < 0 );
						
						if(lastValPositive) {
							assert( min < -0 );
							currAmplitude = -min;
						}
						
						lastValPositive = false;
						min = -0;
					}
					
					
				}
			}

			pipe.setState(State.NULL);
		}
	}

	private class RmsTracker implements Runnable {
		@Override
		public void run() {
			while( ! appSink.isEOS() ) {
				Buffer buf = appSink.pullBuffer();
				
				if( buf == null ) {
					continue;
				}
				
				IntBuffer bBuf = buf.getByteBuffer().
						asIntBuffer().asReadOnlyBuffer();
				
				int i = 0;
				double squareSum = 0.0;
				for( i = 0; bBuf.hasRemaining(); i++ ) {
					double val = bBuf.get(); // convert to double to avoid overflow
					squareSum += (val * val);
				}
				
				currRms = (i == 0) ? 0 : Math.sqrt( squareSum / i ); 
				
				if( Double.isNaN(currRms) ) {
					System.err.printf("NaN! squareSum = %.6f, i = %d%n", squareSum, i);
				}
			}

			pipe.setState(State.NULL);
			System.exit(0);
		}
	}

	
	private class AmplitudeSender implements Runnable {
		private static final int DIVIDE_RATIO = 0x200000;
		
		private int lastVal = -1;
		
		@Override
		public void run() {
			for(;;) {
				// copy because it may be modified by another thread later
				final int ampValueNow = (int)currRms;
								
				if( ampValueNow != lastVal ) {
					final int sendVal = ampValueNow / DIVIDE_RATIO;
//					System.out.printf("%d%n", sendVal);
					packetSender.sendRawByte((byte)sendVal);
					
					lastVal = ampValueNow;
				}
				
				try {
					Thread.sleep(WAIT_MILLIS);
				} catch (InterruptedException e) {
					// allow thread to just be interrupted
				}
			}
			
		}
	}										
	
	

}
