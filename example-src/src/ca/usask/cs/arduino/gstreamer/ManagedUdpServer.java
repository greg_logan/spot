package ca.usask.cs.arduino.gstreamer;

import java.io.IOException;
import java.net.*;

public class ManagedUdpServer extends AbstractUdpServer {
	private static final int READ_BUFFER_LENGTH = 1024;
	private static final int WRITE_BUFFER_LENGTH = 2;
	
	private final Object localMutex = new Object();
	private transient volatile boolean arduinoIsReady = false;
	private volatile InetAddress clientAddr = null;
	private volatile int clientPort = -47;
	
	public ManagedUdpServer( int port ) {
		super(port);
		
//		socket.setSendBufferSize(WRITE_BUFFER_LENGTH);
		
		Thread readyWatch = new Thread(
				new ArduinoInitBlocker(), "Arduino readiness watcher");
		
		readyWatch.start();
	}
	
	/**
	 * Tries to send the specified string through the datagram socket to the
	 * Arduino. Does nothing if the Arduino isn't ready for transmissions.
	 * 
	 * @param s
	 *            The string to send.
	 * @return <code>true</code> if the String was sent, <code>false</code>
	 *         otherwise.
	 */
	public boolean send(byte[] arr) {
		if(!arduinoIsReady) {
			synchronized(localMutex) {
				if(!arduinoIsReady) {
					return false;
				}
			}
		}
		
		try {
			socket.send( new DatagramPacket(
					arr, arr.length, clientAddr, clientPort) );
		} catch (IOException e) {
			// might be able to get away with doing nothing
			e.printStackTrace();
			return false;
		}
		
		System.out.println( (arr.length == 1) ? arr[0] : arr );
		
		return true;
	}
	
	private class ArduinoInitBlocker implements Runnable {
		
		@Override
		public void run() {
			for(;;) {
				
				try {
					DatagramPacket buffer = new DatagramPacket(
							new byte[READ_BUFFER_LENGTH], READ_BUFFER_LENGTH);

					// this method blocks
					socket.receive(buffer);
					
					String strData = new String(
							buffer.getData(), 0, buffer.getLength());
					
					// check to see if the packet is what we want
					if( "Ready".equals(strData) ) {
						socket.send( new DatagramPacket(
								new byte[]{7}, 1, buffer.getAddress(), 
								buffer.getPort() ) );
						
						synchronized(localMutex) {
							arduinoIsReady = true;
							clientAddr = buffer.getAddress();
							clientPort = buffer.getPort();
						}
						
						return;
					}
					
				} catch (IOException e) {
					// I don't think this should be a big deal
					e.printStackTrace();
				}
				
			}
		}
		
	}
	
}
