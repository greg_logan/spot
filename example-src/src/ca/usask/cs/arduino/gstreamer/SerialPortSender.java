package ca.usask.cs.arduino.gstreamer;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.TooManyListenersException;

/*
 * This code makes use of the RXTX library, as obtained from the site 
 * http://rxtx.qbang.org/.
 * 
 * Note that the RXTX library is licensed under LGPL v2.1, with an additional 
 * permission to link the library through the Sun CommAPI. It seems the 
 * license can be found at http://users.frii.com/jarvi/rxtx/license.html 
 * or http://rxtx.qbang.org/wiki/index.php/License.
 * 
 * This code can apparently also be licensed under a later version of the LGPL.
 */
public class SerialPortSender implements BytePacketSender {
	
	private static final int BAUD_RATE = 9600;
	private static final int PORT_OPEN_WAIT_MILLIS = 2000;
	
	private SerialPort port = null;
	private OutputStream os;
	
	public SerialPortSender(String portName) {
		@SuppressWarnings("unchecked")
		List<CommPortIdentifier> l = (List<CommPortIdentifier>)
				Collections.list( CommPortIdentifier.getPortIdentifiers() );
	
		// finds the port we want, and initializes it
		for( CommPortIdentifier ident : l ) {
			if( ident.getName().equals(portName) ) {
				try {
					port = (SerialPort) 
							( ident.open(Thread.currentThread().getName(), 
									PORT_OPEN_WAIT_MILLIS) );
					
					os = port.getOutputStream();
				} catch (PortInUseException e) {
					e.printStackTrace();
					throw new UncooperativePortException(e);
				} catch (IOException e) {
					e.printStackTrace();
					throw new UncooperativePortException(e);
				}
			}
		}
		
		if( port == null ) {
			System.err.println("Port \"" + portName + "\" not found.");
			System.exit(-37);
		}
		
		
		try {
			port.setSerialPortParams(BAUD_RATE, SerialPort.DATABITS_8, 
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			
			port.addEventListener( new SerialPortEventListener() {
				@Override
				public void serialEvent(SerialPortEvent ev) {
					System.out.println( ev.toString() );
				}
			} );
			
//			port.notifyOnOutputEmpty(true);
			port.notifyOnOverrunError(true);
		} catch (UnsupportedCommOperationException e) {
			e.printStackTrace();
			throw new UncooperativePortException(e);
		} catch (TooManyListenersException e) {
			e.printStackTrace();
		}
		

		
		// let's be quite careful to close the COM port
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				SerialPortSender.this.close(); 
				// method from enclosing class
			};
		});
		
		/*try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
	
	}

	@Override
	public boolean send(byte[] arr) {
		try {
			os.write(arr);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		System.out.println( (arr.length == 1) ? arr[0] : arr );
		
		return true;
	}
	
	@Override
	public boolean send(String s) {
		return send(s.getBytes());
	}
	
	@Override
	public boolean sendRawByte(byte b) {
		return send(new byte[]{b});
	}
	
	private synchronized void close() {
		if (port != null) {
			port.removeEventListener();
			port.close();
		}
	}
	
}