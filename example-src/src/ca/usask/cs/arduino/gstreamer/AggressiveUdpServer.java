package ca.usask.cs.arduino.gstreamer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class AggressiveUdpServer extends AbstractUdpServer {
	private static final int WRITE_BUFFER_LENGTH = 2;
	
	private static final InetAddress CLIENT_ADDR;
	private static final int CLIENT_PORT;
	
	static {
		try {
			CLIENT_ADDR = InetAddress.getByName("arduinoaries.usask.ca");
			CLIENT_PORT = 17777;
		} catch (UnknownHostException e) {
			System.err.println(
					"Fatal error: DNS lookup for Arduino failed. Exiting");
			System.exit(0xBAD);
			throw new IllegalStateException(
					"Wow. Epic fail. Exception thrown after system exit.");
		}
	}
	
	public AggressiveUdpServer( int port ) {
		super(port);
		
//		socket.setSendBufferSize(WRITE_BUFFER_LENGTH);
	}
	
	/**
	 * Tries to send the specified string through the datagram socket to the
	 * Arduino.
	 * 
	 * @param s
	 *            The string to send.
	 * @return <code>true</code> if the String was sent, <code>false</code>
	 *         otherwise.
	 */
	@Override
	public boolean send(byte[] arr) {
		try {
			socket.send( new DatagramPacket(
					arr, arr.length, CLIENT_ADDR, CLIENT_PORT) );
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
//		System.out.println( (arr.length == 1) ? arr[0] : arr );
		
		return true;
	}
	
}
