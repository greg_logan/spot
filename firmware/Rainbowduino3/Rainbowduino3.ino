// upload this code to the Rainbowduino!

#include "Rainbow.h"

Rainbow myRainbow = Rainbow();

void setup() {
    Serial.begin(28800);
    myRainbow.init();
    myRainbow.closeAll(); //turn off all LEDs
}

int readChar;
int readLoc;
int readColor;

void loop() {

    if (Serial.available()) {

      readChar = Serial.read();

      if (readChar < 0 || readChar >= 256) {
          return;
      }
      
      if (readChar == 0) {
          myRainbow.closeAll();
      } else {
          int color = ( readChar / 16 ) * 0x111;
          if( color == 0 ) {
            myRainbow.lightOneLine(0, 0xf00, OTHERS_OFF);
            myRainbow.lightOneLine(7, 0xf00, OTHERS_ON);
            myRainbow.lightOneColumn(0, 0xf00, OTHERS_ON);
            myRainbow.lightOneColumn(7, 0xf00, OTHERS_ON);
          } else {
            myRainbow.lightAll(color);
          }
      }

    }

}


