// upload this code to the Arduino Uno!

#include <SPI.h>        
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX pins respectively

byte mac[] = {  0x90, 0xA2, 0xDA, 0x0D, 0x0D, 0xCA }; // device MAC address
unsigned int localPort = 17777; // local port on which to listen for UDP packets

EthernetUDP Udp;

void setup()
{
  Serial.begin(9600);
  mySerial.begin(28800);

  delay(1000);

  // start Ethernet and UDP
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for(;;) {;}
  }
  
  Udp.begin(localPort);
}

unsigned long updateTime = 0;

void loop()
{
  if ( Udp.parsePacket() ) {  

    // pass it along
    
    byte num = Udp.read();
    mySerial.write(num);
    
    updateTime = millis();
//  Serial.println(i);
  } else {
    
    if( millis() - updateTime > 2500 ) { 
      mySerial.write((byte)0); 
    }
    
  }
  
  // parsing packets is fairly exhausting, so we don't make this chip do too much more work
}

