#include "Rainbow.h"

Rainbow myRainbow = Rainbow();

void setup() {
    Serial.begin(28800);
    myRainbow.init();
    myRainbow.closeAll(); //turn off all LEDs
}

char readChar;
int readInt;

void loop() {

    if (Serial.available()) {
//      readChar = Serial.read();
        readInt = Serial.parseInt();
        switch (readInt) {

        case 1:
          myRainbow.lightAll(2986);
          break;
            
        case 0:
          myRainbow.closeAll();  
          break;
          
        default:
          break;

        }

    }
}


