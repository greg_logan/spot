#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX

void setup()  
{
  mySerial.begin(28800);
  delay(5000);
}

#define BIG_DELAY 100

void loop() // run over and over
{
  mySerial.println( "1" );
  digitalWrite(13, HIGH);
  delay(BIG_DELAY);
 
  mySerial.println( "0" );
  digitalWrite(13, LOW);
  delay(BIG_DELAY);
}
