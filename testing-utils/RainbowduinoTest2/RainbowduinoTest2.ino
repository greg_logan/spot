#include "Rainbow.h"

Rainbow myRainbow = Rainbow();

void setup()
{
  Serial.begin(9600);
  myRainbow.init();
  myRainbow.closeAll(); //turn off all LEDs
}

int readLoc;
int readColor;

void loop()
{
    
  if( Serial.available() ) {
       readLoc = Serial.parseInt();
       readColor = Serial.parseInt();
       
       if (readLoc < 0 || readLoc >= 64 || 
           readColor < 0 || readColor >= 4096) {
         return;
       }
       
       int lower = readLoc & 0x7;
       int upper = ( readLoc >> 3 ) & 0x7;
       
       if( readColor == 0 ) {
        myRainbow.closeOneDot( lower, upper );
       } else { 
        myRainbow.lightOneDot( lower, upper, readColor, OTHERS_ON );
       }
  }   
  
//  delay(100);
}
