#include "Rainbow.h"

Rainbow myRainbow = Rainbow();

void setup()
{
  Serial.begin(9600);
  myRainbow.init();
  myRainbow.closeAll();//close all leds
}

char readVal;

void loop()
{
    
  if( Serial.available() ) {
       readVal = Serial.read();
       
       if( readVal == '+' ) {
         myRainbow.lightAll(0x0fff);
       } else if ( readVal == '-' ) {
         myRainbow.closeAll();
       }
  }   
  
  delay(100);
}
