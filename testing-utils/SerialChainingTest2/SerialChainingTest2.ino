#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX

void setup()  
{
  mySerial.begin(28800);


  // seed with an unconnected analog pin for random noise
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  long seed = ( analogRead(A3) << 2 ) * ( analogRead(A4) >> 3 );
  randomSeed(seed);
  
  
  delay(5000);
}

void loop() // run over and over
{
  String s = (String)"g" + ( random() % 4096);
  mySerial.println( s );
  digitalWrite(13, HIGH);
  delay(50);
  digitalWrite(13, LOW);
  delay(50);
  String s2 = (String)"d" + ( random() % 4096 );
  mySerial.println( s2 );
  digitalWrite(13, HIGH);
  delay(50);
  digitalWrite(13, LOW);
  delay(850);
 
}
