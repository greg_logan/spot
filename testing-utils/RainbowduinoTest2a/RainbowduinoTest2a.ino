#include "Rainbow.h"

Rainbow myRainbow = Rainbow();

void setup() {
    Serial.begin(28800);
    myRainbow.init();
    myRainbow.closeAll(); //turn off all LEDs
}

int readChar;
int readLoc;
int readColor;

void loop() {

    if (Serial.available()) {

        int lower, upper;

        readChar = Serial.read();
        switch (readChar) {

        case '(':
            readLoc = Serial.parseInt();
            readColor = Serial.parseInt();

            if (readLoc < 0 || readLoc >= 64 || readColor < 0
                    || readColor >= 4096) {
                return;
            }

            lower = readLoc & 0x7;
            upper = ((unsigned int) readLoc >> 3) & 0x7;

            if (readColor == 0) {
                myRainbow.closeOneDot(lower, upper);
            } else {
                myRainbow.lightOneDot(lower, upper, readColor, OTHERS_ON);
            }
            break;
      
        case '+':
            readColor = Serial.parseInt();

            if (readColor < 0 || readColor >= 4096) {
                return;
            }

            /*if (readColor == 0) {
                myRainbow.closeAll();
            } else {
            */
                myRainbow.lightAll(readColor);
        //  }
            break;
            
        case '~':
        case '|':
            readLoc = Serial.parseInt();
            readColor = Serial.parseInt();

            if (readLoc < 0 || readLoc > 7 ||
                    readColor < 0 || readColor >= 4096) {
                return;
            }
            
            if( readChar == '~' ) { 
                myRainbow.lightOneLine(readLoc, readColor, OTHERS_ON);
            } else {
                myRainbow.lightOneColumn(readLoc, readColor, OTHERS_ON);
            }
            
            break;
            
        case 'g':
        case 'd':
            readColor = Serial.parseInt();

            if (readColor < 0 || readColor >= 4096) {
                return;
            }

/*          if (readColor == 0) {
                myRainbow.closeOneLine(4);
                myRainbow.closeOneLine(5);
                myRainbow.closeOneLine(6);
                myRainbow.closeOneLine(7);
            } else {*/
            
            if( readChar == 'g' ) {
                myRainbow.lightOneColumn(4, readColor, OTHERS_ON);
                myRainbow.lightOneColumn(5, readColor, OTHERS_ON);
                myRainbow.lightOneColumn(6, readColor, OTHERS_ON);
                myRainbow.lightOneColumn(7, readColor, OTHERS_ON);
            } else {
                myRainbow.lightOneColumn(0, readColor, OTHERS_ON);
                myRainbow.lightOneColumn(1, readColor, OTHERS_ON);
                myRainbow.lightOneColumn(2, readColor, OTHERS_ON);
                myRainbow.lightOneColumn(3, readColor, OTHERS_ON);
            }
            break;
            
        default:
            break;

        }

    }


}


