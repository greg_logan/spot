#include <SPI.h>        
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(5, 6); // RX, TX

byte mac[] = {  0x90, 0xA2, 0xDA, 0x0D, 0x0D, 0xCA }; // device MAC address
unsigned int localPort = 17777; // local port on which to listen for UDP packets

#define PACKET_SIZE 80
byte packetBuffer[PACKET_SIZE]; // buffer to hold incoming and outgoing packets

// attempt at getting static IP of a dynamic IPed computer :(
IPAddress server(128, 233, 104, 136); 
char myUrl[] = "theta_octans.usask.ca";

EthernetUDP Udp;

void setup()
{
  Serial.begin(9600);
  mySerial.begin(28800);

  delay(10000);

  // start Ethernet and UDP
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for(;;) {;}
  }
  
  Udp.begin(localPort);
  
  init(server);
}

void loop()
{
  // wait to see if a reply is available
  delay(1000);  
  if ( Udp.parsePacket() ) {  
    // We've received a packet, read the data from it
//    Udp.read(packetBuffer,PACKET_SIZE);  // read the packet into the buffer

    int i = Udp.parseInt();
    Serial.println(i);
  }
}

char initQuery[] = "Ready";

void init(IPAddress& address)
{
  for(;;) {
    
    Udp.beginPacket(address, 17777);
    Udp.write(initQuery);
    Udp.endPacket();
    Serial.println("Trying.");
    
    delay(1000);
    if( Udp.parsePacket() ) {
      char c = Udp.read();
      
      if( c == 7 ) {
        Serial.println("Connected.");
        return;
      }
      
      delay(1000);
    }
  
  }
}

void initUrl(char url[]) {
  for(;;) {
    
    Udp.beginPacket(url, 17777);
    Udp.write(initQuery);
    Udp.endPacket();
    Serial.println("Trying.");
    
    delay(1000);
    if( Udp.parsePacket() ) {
      char c = Udp.read();
      
      if( c == 7 ) {
        Serial.println("Connected.");
        return;
      }
      
      delay(1000);
    }
  
  }
}
